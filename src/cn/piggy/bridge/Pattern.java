package cn.piggy.bridge;

public interface Pattern {
    String getPattern();
}
