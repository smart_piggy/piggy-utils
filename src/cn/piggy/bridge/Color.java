package cn.piggy.bridge;

public interface Color {
    String getColor();
}
