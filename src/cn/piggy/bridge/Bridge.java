package cn.piggy.bridge;

public class Bridge {
    private final Color color;
    private final Pattern pattern;

    public Bridge(Color color, Pattern pattern) {
        this.color = color;
        this.pattern = pattern;
    }

    public String execute(){
        return color.getColor()+" "+pattern.getPattern();
    }

    public static void main(String[] args) {
        Red red = new Red();
        Square square = new Square();
        Bridge bridge = new Bridge(red, square);
        String execute = bridge.execute();
        System.out.println(execute);
    }
}
