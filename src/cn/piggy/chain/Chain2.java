package cn.piggy.chain;

public class Chain2 {
    public static void main(String[] args) {
        HandlerA handlerA = new HandlerA();
        HandlerB handlerB = new HandlerB();
        HandlerC handlerC = new HandlerC();
        handlerA.setHandler(handlerB);
        handlerB.setHandler(handlerC);
        handlerA.doHandler(1);
    }
}


interface IHandler{
    void doHandler(Integer number);
}


abstract class Handler implements IHandler {
    private Handler handler;

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public abstract void doHandler(Integer number);
}

class HandlerA extends Handler{
    @Override
    public void doHandler(Integer number) {
        Handler handler = this.getHandler();
        System.out.println("HandlerA: "+number);
        if(handler!=null){
            number++;
            handler.doHandler(number);
        }
    }
}

class HandlerB extends Handler{
    @Override
    public void doHandler(Integer number) {
        Handler handler = this.getHandler();
        System.out.println("HandlerB: "+number);
        if(handler!=null){
            number++;
            handler.doHandler(number);
        }
    }
}

class HandlerC extends Handler{
    @Override
    public void doHandler(Integer number) {
        Handler handler = this.getHandler();
        System.out.println("HandlerC: "+number);
        if(handler!=null){
            number++;
            handler.doHandler(number);
        }
    }
}