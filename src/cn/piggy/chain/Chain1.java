package cn.piggy.chain;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Chain1 {
    public static void main(String[] args) {
        FilterChain chain = new FilterChain();
        chain.addFilter(new FilterA());
        chain.addFilter(new FilterB());
        chain.addFilter(new FilterC());
        chain.processData();
    }
}

interface IFilter {
    void doFilter(Integer data); // 过滤操作
}

// 过滤器A
class FilterA implements IFilter {
    @Override
    public void doFilter(Integer data) {
        System.out.println("Filter A: " + data);
    }
}

// 过滤器B
class FilterB implements IFilter {
    @Override
    public void doFilter(Integer data) {
        System.out.println("Filter B: " + data);
    }
}

// 过滤器C
class FilterC implements IFilter {
    @Override
    public void doFilter(Integer data) {
        System.out.println("Filter C: " + data);
    }
}

// 过滤器链
class FilterChain {
    private final List<IFilter> filters = new CopyOnWriteArrayList<>();
    private Integer integer = 1;
    public FilterChain() {}

    public void addFilter(IFilter filter) {
        filters.add(filter);
    }

    public void processData() {
        for (IFilter filter : filters) {
            filter.doFilter(integer++);
        }
    }
}