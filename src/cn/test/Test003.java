package cn.test;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class Test003 {
    private static class QuickSortTask extends RecursiveAction {
        private static final int THRESHOLD = 2;
        private final int[] array;
        private final int left;
        private final int right;

        public QuickSortTask(int[] array, int left, int right) {
            this.array = array;
            this.left = left;
            this.right = right;
        }

        @Override
        protected void compute() {
            if (right - left <= THRESHOLD) {
                Arrays.sort(array, left, right + 1);
            } else {
                int pivotIndex = partition(array, left, right);
                QuickSortTask leftTask = new QuickSortTask(array, left, pivotIndex - 1);
                QuickSortTask rightTask = new QuickSortTask(array, pivotIndex + 1, right);
                invokeAll(leftTask, rightTask);
            }
        }

        private int partition(int[] array, int left, int right) {
            int pivot = array[right];
            int i = left - 1;
            for (int j = left; j < right; j++) {
                if (array[j] < pivot) {
                    i++;
                    swap(array, i, j);
                }
            }
            swap(array, i + 1, right);
            return i + 1;
        }

        private void swap(int[] array, int i, int j) {
            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    public static void parallelQuickSort(int[] array) {
        ForkJoinPool pool = new ForkJoinPool();
        QuickSortTask task = new QuickSortTask(array, 0, array.length - 1);
        pool.invoke(task);
    }

    public static void main(String[] args) {
        int[] array = {10, 9, 3, 1, 7, 5, 6, 2, 8, 4};
        parallelQuickSort(array);
        System.out.println(Arrays.toString(array));
    }
}
