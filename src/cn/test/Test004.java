package cn.test;

import cn.piggy.GenericQuickSort;

import java.util.ArrayList;
import java.util.List;

public class Test004 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("fff");
        list.add("eee");
        list.add("ddd");
        list.add("ccc");
        list.add("bbb");
        list.add("aaa");
        list.add("fff");
        list.add("eee");
        list.add("ddd");
        list.add("ccc");
        list.add("bbb");
        list.add("eee");
        list.add("ddd");
        list.add("ccc");
        list.add("bbb");
        list.add("aaa");
        list.add("fff");
        list.add("aaa");
        list.add("bbb");
        list.add("aaa");
        list.add("fff");
        list.add("eee");
        list.add("ddd");
        GenericQuickSort.piggyQuick(list);
        System.out.println(list);
    }
}
