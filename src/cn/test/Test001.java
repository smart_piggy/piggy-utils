package cn.test;

import cn.piggy.PiggyMergeSort;
import cn.piggy.PiggyQuickSort;

import java.util.Arrays;
import java.util.Random;

public class Test001 {
    public static void main(String[] args) {

        int[] int1 = randomArray();
        long l1 = System.currentTimeMillis();
        Arrays.sort(int1);
        long l2 = System.currentTimeMillis();
        System.out.println("system ---> "+(l2-l1));

        int[] int2 = randomArray();
        long l3 = System.currentTimeMillis();
        PiggyMergeSort.piggyMerge(int2);
        long l4 = System.currentTimeMillis();
        System.out.println("merge ---> "+(l4-l3));

        int[] int3 = randomArray();
        long l5 = System.currentTimeMillis();
        PiggyQuickSort.piggyQuick(int3);
        long l6 = System.currentTimeMillis();
        System.out.println("quick ---> "+(l6-l5));


    }
    private static int[] randomArray() {
        int[] array = new int[100000000];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100000000);
        }
        return array;
    }
}
