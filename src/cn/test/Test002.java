package cn.test;

import cn.piggy.PiggyQuickSort;
import java.util.Arrays;
import java.util.Random;

public class Test002 {
    public static void main(String[] args) {
        int[] int3 = Test002.randomArray();
        int[] int4 = Test002.randomArray();

        new Thread(() -> {
            PiggyQuickSort.piggyQuick(int3);
            System.out.println(Arrays.toString(int3));
        }).start();
        new Thread(() -> {
            PiggyQuickSort.piggyQuick(int4);
            System.out.println(Arrays.toString(int4));
        }).start();


    }

    public static int[] randomArray() {
        int[] array = new int[100];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(1000);
        }
        return array;
    }
}
